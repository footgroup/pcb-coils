EESchema Schematic File Version 4
LIBS:single_coil-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L antenna:antenna a1
U 1 1 5F637BA1
P 4700 3500
F 0 "a1" H 4725 3825 50  0000 C CNN
F 1 "antenna" H 4725 3734 50  0000 C CNN
F 2 "antenna:antenna" H 4700 3450 50  0001 C CNN
F 3 "" H 4700 3450 50  0001 C CNN
	1    4700 3500
	1    0    0    -1  
$EndComp
$Comp
L antenna:antenna a2
U 1 1 5F63819C
P 6100 3500
F 0 "a2" H 6125 3825 50  0000 C CNN
F 1 "antenna" H 6125 3734 50  0000 C CNN
F 2 "antenna:antenna_2" H 6100 3450 50  0001 C CNN
F 3 "" H 6100 3450 50  0001 C CNN
	1    6100 3500
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J1
U 1 1 5F63890A
P 3750 3400
F 0 "J1" H 3678 3638 50  0000 C CNN
F 1 "Conn_Coaxial" H 3678 3547 50  0000 C CNN
F 2 "Connector_Coaxial:SMA_Molex_73251-2200_Horizontal" H 3750 3400 50  0001 C CNN
F 3 " ~" H 3750 3400 50  0001 C CNN
	1    3750 3400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3950 3400 4450 3400
Wire Wire Line
	4950 3400 5850 3400
Wire Wire Line
	6350 3400 6350 3750
Wire Wire Line
	6350 3750 3750 3750
Wire Wire Line
	3750 3750 3750 3600
$Comp
L SamacSys_Parts:3-1478978-1 J2
U 1 1 60B78F74
P 2450 2350
F 0 "J2" H 2850 2615 50  0000 C CNN
F 1 "3-1478978-1" H 2850 2524 50  0000 C CNN
F 2 "3-1478978-1" H 3100 2450 50  0001 L CNN
F 3 "http://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Customer+Drawing%7F1478978%7FD%7Fpdf%7FEnglish%7FENG_CD_1478978_D.pdf%7F3-1478978-1" H 3100 2350 50  0001 L CNN
F 4 "SMA right angle PCB socket TE Connectivity Right Angle 50 Through Hole SMA Connector, jack, Solder Termination Coaxial" H 3100 2250 50  0001 L CNN "Description"
F 5 "TE Connectivity" H 3100 2050 50  0001 L CNN "Manufacturer_Name"
F 6 "3-1478978-1" H 3100 1950 50  0001 L CNN "Manufacturer_Part_Number"
F 7 "571-3-1478978-1" H 3100 1850 50  0001 L CNN "Mouser Part Number"
F 8 "https://www.mouser.co.uk/ProductDetail/TE-Connectivity/3-1478978-1?qs=Vcr9%2FL0R50iRERB%252Br7Gnqw%3D%3D" H 3100 1750 50  0001 L CNN "Mouser Price/Stock"
F 9 "3-1478978-1" H 3100 1650 50  0001 L CNN "Arrow Part Number"
F 10 "https://www.arrow.com/en/products/3-1478978-1/te-connectivity" H 3100 1550 50  0001 L CNN "Arrow Price/Stock"
	1    2450 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	2450 2550 2450 2450
Connection ~ 2450 2450
Wire Wire Line
	2450 2450 2450 2350
$Comp
L power:GND #PWR0101
U 1 1 60B79735
P 2450 2550
F 0 "#PWR0101" H 2450 2300 50  0001 C CNN
F 1 "GND" H 2455 2377 50  0000 C CNN
F 2 "" H 2450 2550 50  0001 C CNN
F 3 "" H 2450 2550 50  0001 C CNN
	1    2450 2550
	1    0    0    -1  
$EndComp
Connection ~ 2450 2550
$Comp
L power:GND #PWR0102
U 1 1 60B7A1EC
P 3250 2350
F 0 "#PWR0102" H 3250 2100 50  0001 C CNN
F 1 "GND" V 3255 2222 50  0000 R CNN
F 2 "" H 3250 2350 50  0001 C CNN
F 3 "" H 3250 2350 50  0001 C CNN
	1    3250 2350
	0    -1   -1   0   
$EndComp
$Comp
L SamacSys_Parts:3-1478978-1 J3
U 1 1 60B7C33F
P 4050 1300
F 0 "J3" H 4450 1565 50  0000 C CNN
F 1 "3-1478978-1" H 4450 1474 50  0000 C CNN
F 2 "3-1478978-1" H 4700 1400 50  0001 L CNN
F 3 "http://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Customer+Drawing%7F1478978%7FD%7Fpdf%7FEnglish%7FENG_CD_1478978_D.pdf%7F3-1478978-1" H 4700 1300 50  0001 L CNN
F 4 "SMA right angle PCB socket TE Connectivity Right Angle 50 Through Hole SMA Connector, jack, Solder Termination Coaxial" H 4700 1200 50  0001 L CNN "Description"
F 5 "TE Connectivity" H 4700 1000 50  0001 L CNN "Manufacturer_Name"
F 6 "3-1478978-1" H 4700 900 50  0001 L CNN "Manufacturer_Part_Number"
F 7 "571-3-1478978-1" H 4700 800 50  0001 L CNN "Mouser Part Number"
F 8 "https://www.mouser.co.uk/ProductDetail/TE-Connectivity/3-1478978-1?qs=Vcr9%2FL0R50iRERB%252Br7Gnqw%3D%3D" H 4700 700 50  0001 L CNN "Mouser Price/Stock"
F 9 "3-1478978-1" H 4700 600 50  0001 L CNN "Arrow Part Number"
F 10 "https://www.arrow.com/en/products/3-1478978-1/te-connectivity" H 4700 500 50  0001 L CNN "Arrow Price/Stock"
	1    4050 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 1500 4050 1400
Connection ~ 4050 1400
Wire Wire Line
	4050 1400 4050 1300
$Comp
L power:GND #PWR0103
U 1 1 60B7C348
P 4050 1500
F 0 "#PWR0103" H 4050 1250 50  0001 C CNN
F 1 "GND" H 4055 1327 50  0000 C CNN
F 2 "" H 4050 1500 50  0001 C CNN
F 3 "" H 4050 1500 50  0001 C CNN
	1    4050 1500
	1    0    0    -1  
$EndComp
Connection ~ 4050 1500
$Comp
L power:GND #PWR0104
U 1 1 60B7C34F
P 4850 1300
F 0 "#PWR0104" H 4850 1050 50  0001 C CNN
F 1 "GND" V 4855 1172 50  0000 R CNN
F 2 "" H 4850 1300 50  0001 C CNN
F 3 "" H 4850 1300 50  0001 C CNN
	1    4850 1300
	0    -1   -1   0   
$EndComp
$Comp
L SamacSys_Parts:3-1478978-1 J4
U 1 1 60B7C780
P 5800 1800
F 0 "J4" H 6200 2065 50  0000 C CNN
F 1 "3-1478978-1" H 6200 1974 50  0000 C CNN
F 2 "3-1478978-1" H 6450 1900 50  0001 L CNN
F 3 "http://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Customer+Drawing%7F1478978%7FD%7Fpdf%7FEnglish%7FENG_CD_1478978_D.pdf%7F3-1478978-1" H 6450 1800 50  0001 L CNN
F 4 "SMA right angle PCB socket TE Connectivity Right Angle 50 Through Hole SMA Connector, jack, Solder Termination Coaxial" H 6450 1700 50  0001 L CNN "Description"
F 5 "TE Connectivity" H 6450 1500 50  0001 L CNN "Manufacturer_Name"
F 6 "3-1478978-1" H 6450 1400 50  0001 L CNN "Manufacturer_Part_Number"
F 7 "571-3-1478978-1" H 6450 1300 50  0001 L CNN "Mouser Part Number"
F 8 "https://www.mouser.co.uk/ProductDetail/TE-Connectivity/3-1478978-1?qs=Vcr9%2FL0R50iRERB%252Br7Gnqw%3D%3D" H 6450 1200 50  0001 L CNN "Mouser Price/Stock"
F 9 "3-1478978-1" H 6450 1100 50  0001 L CNN "Arrow Part Number"
F 10 "https://www.arrow.com/en/products/3-1478978-1/te-connectivity" H 6450 1000 50  0001 L CNN "Arrow Price/Stock"
	1    5800 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 2000 5800 1900
Connection ~ 5800 1900
Wire Wire Line
	5800 1900 5800 1800
$Comp
L power:GND #PWR0105
U 1 1 60B7C789
P 5800 2000
F 0 "#PWR0105" H 5800 1750 50  0001 C CNN
F 1 "GND" H 5805 1827 50  0000 C CNN
F 2 "" H 5800 2000 50  0001 C CNN
F 3 "" H 5800 2000 50  0001 C CNN
	1    5800 2000
	1    0    0    -1  
$EndComp
Connection ~ 5800 2000
$Comp
L power:GND #PWR0106
U 1 1 60B7C790
P 6600 1800
F 0 "#PWR0106" H 6600 1550 50  0001 C CNN
F 1 "GND" V 6605 1672 50  0000 R CNN
F 2 "" H 6600 1800 50  0001 C CNN
F 3 "" H 6600 1800 50  0001 C CNN
	1    6600 1800
	0    -1   -1   0   
$EndComp
$Comp
L SamacSys_Parts:3-1478978-1 J5
U 1 1 60B7D06F
P 7150 1900
F 0 "J5" H 7550 2165 50  0000 C CNN
F 1 "3-1478978-1" H 7550 2074 50  0000 C CNN
F 2 "3-1478978-1" H 7800 2000 50  0001 L CNN
F 3 "http://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Customer+Drawing%7F1478978%7FD%7Fpdf%7FEnglish%7FENG_CD_1478978_D.pdf%7F3-1478978-1" H 7800 1900 50  0001 L CNN
F 4 "SMA right angle PCB socket TE Connectivity Right Angle 50 Through Hole SMA Connector, jack, Solder Termination Coaxial" H 7800 1800 50  0001 L CNN "Description"
F 5 "TE Connectivity" H 7800 1600 50  0001 L CNN "Manufacturer_Name"
F 6 "3-1478978-1" H 7800 1500 50  0001 L CNN "Manufacturer_Part_Number"
F 7 "571-3-1478978-1" H 7800 1400 50  0001 L CNN "Mouser Part Number"
F 8 "https://www.mouser.co.uk/ProductDetail/TE-Connectivity/3-1478978-1?qs=Vcr9%2FL0R50iRERB%252Br7Gnqw%3D%3D" H 7800 1300 50  0001 L CNN "Mouser Price/Stock"
F 9 "3-1478978-1" H 7800 1200 50  0001 L CNN "Arrow Part Number"
F 10 "https://www.arrow.com/en/products/3-1478978-1/te-connectivity" H 7800 1100 50  0001 L CNN "Arrow Price/Stock"
	1    7150 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 2100 7150 2000
Connection ~ 7150 2000
Wire Wire Line
	7150 2000 7150 1900
$Comp
L power:GND #PWR0107
U 1 1 60B7D078
P 7150 2100
F 0 "#PWR0107" H 7150 1850 50  0001 C CNN
F 1 "GND" H 7155 1927 50  0000 C CNN
F 2 "" H 7150 2100 50  0001 C CNN
F 3 "" H 7150 2100 50  0001 C CNN
	1    7150 2100
	1    0    0    -1  
$EndComp
Connection ~ 7150 2100
$Comp
L power:GND #PWR0108
U 1 1 60B7D07F
P 7950 1900
F 0 "#PWR0108" H 7950 1650 50  0001 C CNN
F 1 "GND" V 7955 1772 50  0000 R CNN
F 2 "" H 7950 1900 50  0001 C CNN
F 3 "" H 7950 1900 50  0001 C CNN
	1    7950 1900
	0    -1   -1   0   
$EndComp
$EndSCHEMATC
