EESchema Schematic File Version 4
LIBS:single_coil-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L antenna:antenna a1
U 1 1 5F637BA1
P 4700 3500
F 0 "a1" H 4725 3825 50  0000 C CNN
F 1 "antenna" H 4725 3734 50  0000 C CNN
F 2 "antenna:antenna" H 4700 3450 50  0001 C CNN
F 3 "" H 4700 3450 50  0001 C CNN
	1    4700 3500
	1    0    0    -1  
$EndComp
$Comp
L antenna:antenna a2
U 1 1 5F63819C
P 6100 3500
F 0 "a2" H 6125 3825 50  0000 C CNN
F 1 "antenna" H 6125 3734 50  0000 C CNN
F 2 "antenna:antenna_2" H 6100 3450 50  0001 C CNN
F 3 "" H 6100 3450 50  0001 C CNN
	1    6100 3500
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial J1
U 1 1 5F63890A
P 3750 3400
F 0 "J1" H 3678 3638 50  0000 C CNN
F 1 "Conn_Coaxial" H 3678 3547 50  0000 C CNN
F 2 "Connector_Coaxial:SMA_Molex_73251-2200_Horizontal" H 3750 3400 50  0001 C CNN
F 3 " ~" H 3750 3400 50  0001 C CNN
	1    3750 3400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3950 3400 4450 3400
Wire Wire Line
	4950 3400 5850 3400
Wire Wire Line
	6350 3400 6350 3750
Wire Wire Line
	6350 3750 3750 3750
Wire Wire Line
	3750 3750 3750 3600
$EndSCHEMATC
